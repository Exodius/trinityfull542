///////////////////////////////////////////////////////////////////////////////////////
///////																			///////
///////		Initial Base script for Stage #1 of Brawler's Guild Gizmo's Pub.	///////
///////																			///////
///////////////////////////////////////////////////////////////////////////////////////


#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"

enum bosses
{
	BOSS_GOREDOME,
	BOSS_SANORIAK,
	BOSS_HOOFSTOMP,
	BOSS_AKAMA
};

#define HEALTH_GOREDOME = 415880
#define HEALTH_SANORIAK = 489270
#define HEALTH_HOOFSTOMP = 1957080
#define HEALTH_AKAMA = 538197

enum spells
{
	// Sanoriak Spells
	SPELL_FIREBALL = 136333,
	SPELL_FIREWALL = 132666,
	SPELL_FLAME_BUFFET = 135237,
	SPELL_HEATED_WEAPON = 135236,

	// Hoofstomp Spells
	SPELL_HOOF_STOMP = 134623,

	// Akama Spells
	SPELL_CHAIN_LIUGHTNING = 139963,
	SPELL_FERAL_SPIRIT = 136194,
	SPELL_SHADOW_STRIKES = 126209,
	SPELL_THUNDERSTORM = 79797,

};



class boss_goredome : public CreatureScript
{
public:
	boss_goredome() : CreatureScript("boss_goredome") { }

	CreatureAI* GetAI(Creature* creature) const
	{
		return new boss_goredomeAI(creature);
	}


	struct boss_goredomeAI : public ScriptedAI
	{
		boss_goredomeAI(Creature* creature) : ScriptedAI(creature) {}


		void Reset() {}
		void EnterCombat(Unit* /*who*/) {}
		void AttackStart(Unit* /*who*/) {}
		void MoveInLineOfSight(Unit* /*who*/) {}
		void UpdateAI(const uint32 /*diff*/)
		{
			//Return since we have no target
			if (!UpdateVictim())
				return;

			DoMeleeAttackIfReady();
		}
		void JustDied(Unit* /*killer*/)  {}
	};

};

void AddSC_bosses_brawlerone()
{
	new boss_goredome();
}
