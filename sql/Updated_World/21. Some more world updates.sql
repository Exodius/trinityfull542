-- Add missing three mounts to vendor Jaluu the Generous
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (59908, 0, 87781, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (59908, 0, 87782, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (59908, 0, 87783, 0, 0, 0, 1);

-- Add missing three mounts to Rushi The Fox
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64595, 0, 89305, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64595, 0, 89306, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64595, 0, 89307, 0, 0, 0, 1);

-- Add Missing three mounts to San Redscale
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58414, 0, 79802, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58414, 0, 85429, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58414, 0, 85430, 0, 0, 0, 1);
